import Button from "./AppButton";
import { sdk } from "../lib/sdk";

const TodoCard = function({ todo, hideDeletion }) {
    const defaultCard = document.createElement('div');
    defaultCard.innerHTML = `
        <div class="todos">
            <article class="todo">
                <a>
                    <header>
                        <h1></h1>
                    </header>
                    <main>
                        <p></p>
                        <small></small>
                    </main>
                </a>        
                <footer>
                </footer>
            </article>
        </div>
    `;      // Default card template

    const card = defaultCard
      .querySelector('article.todo')
      .cloneNode(true);

    card.setAttribute('id', `todo-${todo.id}`);

    const title = card.querySelector('h1');
    title.innerHTML = todo.content;
    
    const description = card.querySelector('p');
    description.innerHTML = todo.done ? 'Fait' : 'A faire';
    
    const info = card.querySelector('small');
    info.innerHTML = todo.is_sync ? 'Synchronisé' : 'Non synchronisé';
    info.classList.add((todo.is_sync ? 'sync' : 'not_sync'));
    
    const footer = card.querySelector('footer');
    if (!hideDeletion) {
        footer.append( Button({title: 'Supprimer', onClick: async e => {
            if (window.confirm('Etes vous certains de vouloir supprimer ce todo ?')) {
                const deletedItem = await sdk.todo(todo.id).deleteItem();;
                console.log(deletedItem);
                if (!!deletedItem) {
                    document.getElementById(`todo-${todo.id}`).remove();
                }
            }
        } }));
    } else {
        footer.remove();
    }

    const link = card.querySelector('a');
    link.href = `/todos/${todo.id}`;

    return card;
}

export default TodoCard;