import TodoCard from "../components/TodoCard";
import AppHeader from "../components/AppHeader";
import Button from "../components/AppButton";
import { sdk } from "../lib/sdk";

export default function TodosShow(page, todo) {

    page.appendChild( AppHeader({ title: todo.content.slice(0, 20) + '...'}) );
    
    page.appendChild( TodoCard({ todo, hideDeletion: true }) );

    page.appendChild( Button({
        title: !todo.done ? 'Mark as done' : 'Undo this task',
        onClick: e => {
            sdk.todo(todo.id).patchItem({...todo, done: !todo.done})
                .then(data => data.json())
                .then(() => {
                    window.location.href = '/todos';
                })
                .catch(error => alert('Impossible de charger la donnée... Veuillez rafraichir'));
        }
    }) )
}