import TodoCard from "../components/TodoCard";
import AppHeader from "../components/AppHeader";
import Button from "../components/AppButton";

export default function TodosList(page, todos) {

    page.appendChild( AppHeader({ title: 'All todos'}) );
    
    todos.map(todo => {
        
        page.appendChild( TodoCard({ todo }) );

    });
    
    page.appendChild( Button({
        title: 'Add a todo',
        onClick: e => window.location.href = '/todos/create',
    }) )
}