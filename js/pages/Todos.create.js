import AppHeader from "../components/AppHeader";
import Button from "../components/AppButton";
import { sdk } from "../lib/sdk";

export default function TodosShow(page) {

    page.appendChild( AppHeader({ title: 'Create a new todo' }) );
    const form = document.createElement('form');

    const input = `
        <label>
            Todo content
            <input type="text name="content" />
        </label>
    `;
    form.innerHTML = input;

    page.appendChild(form);

    page.appendChild( Button({
        title: 'Ajouter',
        onClick: async e => {
            e.preventDefault();
            const content = form.querySelector('input').value;

            const todo = {
                id: Date.now(),
                content,
                done: false,
            };

            await sdk.todos.postItem(todo);
            // Redirect to location
        }
    }) );
}