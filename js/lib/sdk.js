import { request } from './request';
import { IS_ONLINE } from '../network';
import { connectDB } from './indexedDB';

const URL_API = 'http://localhost:3000';

export const sdk = {
    todos: {
        getCollection: () => request.get(`${URL_API}/todos`),
        postItem: (item) => {
            dbConnector.connect('todos')
                .create(item, 
                    item => request.post(`${URL_API}/todos`, item).then(() => window.location.href = '/todos')
                );
        },
    },
    todo : id => ({
        getItem: () => request.get(`${URL_API}/todos/${id}`),
        deleteItem: () => request.delete(`${URL_API}/todos/${id}`),
        patchItem: (todo) => request.patch(`${URL_API}/todos/${id}`, todo),
    }),
};

// DB Connector
export const dbConnector = {
    connect: ressource => ({
        create: (item, cb) => {
            connectDB(ressource, (store, tx) => {
                //
                item.is_sync = IS_ONLINE;
                store.add(item);
                //
                tx.oncomplete = function() { 
                    if (IS_ONLINE) {
                        cb(item);
                    } else {
                        return true;
                    }
                }
                tx.onerror = function(event) {
                    alert('error storing note ' + event.target.errorCode);
                }
            })
        }
    }),
};