let db;
let dbSDK;

export const connectDB = (ressource, cb) => {
    let request = window.indexedDB.open("TodoApp");
    //
    request.onerror = function(event) {
      alert("Pourquoi ne permettez-vous pas à ma web app d'utiliser IndexedDB?!");
    };
    //
    request.onsuccess = function(event) {
      db = event.target.result;
      let tx = db.transaction([ressource], 'readwrite');
      let store = tx.objectStore(ressource);
      if (!!cb) cb(store, tx);
    };
    //
    request.onupgradeneeded = function(event) { 
        db = event.target.result;  
        // Create todos object
        db.createObjectStore("todos", {autoIncrement: true});
    };
}