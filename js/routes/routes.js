import { sdk } from "../lib/sdk";

export const routes = [
    {
        path: '/todos',
        pageName: 'Todos.list',
        section: 'todos_list',
        title: 'My todos',
        loadData: () => sdk.todos.getCollection()
    },
    {
        path: '/todos/create',
        pageName: 'Todos.create',
        section: 'todos_create',
        title: 'Create a brand new todo'
    },
    {
        path: '/todos/:id',
        pageName: 'Todos.show',
        section: 'todos_show',
        title: todo => todo.content,
        loadData: ({ id }) => sdk.todo(id).getItem()
    },
];